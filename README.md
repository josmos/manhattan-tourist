# Manhattan tourist problem

This program computes the maximum edge weight of a "Manhattan" graph. 
The input FILE contains 2 or 3 (for diagonal edges) matrices with a header line and float or integer values for the edge weights. 
Matrices should be ordererd down -> right -> diagonal and separated wit "---"

## Usage:

manhattan_tourist_visualized.py -i FILE


## Requirements:

- Python3
- matplotlib (pip install)
- networkx   (pip install)

